package pl.jurczak.kamil.dao;

import org.hibernate.Transaction;
import pl.jurczak.kamil.entity.User;
import pl.jurczak.kamil.util.HibernateUtil;

import java.util.List;

public class UserDAOImpl implements UserDAO {

	@Override
	public User create(final User user) {
		Transaction transaction = null;
		try (var session = HibernateUtil.getSessionFactory().openSession()) {
			transaction = session.beginTransaction();
			session.save(user);
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public User read(final Long id) {
		return null;
	}

	@Override
	public boolean update(final User user) {
		return false;
	}

	@Override
	public boolean delete(final Long id) {
		return false;
	}

	@Override
	public List<User> getAll() {
		return null;
	}
}
