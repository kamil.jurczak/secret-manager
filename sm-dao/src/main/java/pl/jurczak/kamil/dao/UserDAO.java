package pl.jurczak.kamil.dao;

import pl.jurczak.kamil.entity.User;

public interface UserDAO extends GenericDAO<User, Long> {

	@Override
	User create(User user);
}
