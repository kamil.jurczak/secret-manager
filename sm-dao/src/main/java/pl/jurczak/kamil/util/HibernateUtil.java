package pl.jurczak.kamil.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import pl.jurczak.kamil.entity.User;

import java.util.Properties;

import static org.hibernate.cfg.Environment.*;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class HibernateUtil {

	private static SessionFactory sessionFactory;


	public static SessionFactory getSessionFactory() {
		if (sessionFactory == null) {
			try {
				final var configuration = new Configuration();
				final var settings = new Properties();
				settings.put(DRIVER, "org.postgresql.Driver");
				settings.put(URL, "jdbc:postgresql://localhost:5432/secrets?useSSL=false");

				final var authProperties = AuthPropertiesResolver.getSuperUserProperties();
				settings.put(USER, authProperties.getProperty("db.user"));
				settings.put(PASS, authProperties.getProperty("db.password"));

				settings.put(DIALECT, "org.hibernate.dialect.PostgreSQL95Dialect");
				settings.put(SHOW_SQL, "true");
				settings.put(CURRENT_SESSION_CONTEXT_CLASS, "thread");
				settings.put(HBM2DDL_AUTO, "create-drop");
				configuration.setProperties(settings);
				configuration.addAnnotatedClass(Class.class);
				configuration.addAnnotatedClass(User.class);
				final var serviceRegistry = new StandardServiceRegistryBuilder()
						.applySettings(configuration.getProperties()).build();
				sessionFactory = configuration.buildSessionFactory(serviceRegistry);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return sessionFactory;
	}
}
