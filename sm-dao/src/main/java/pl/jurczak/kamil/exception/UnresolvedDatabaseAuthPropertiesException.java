package pl.jurczak.kamil.exception;

public class UnresolvedDatabaseAuthPropertiesException extends RuntimeException {

	public UnresolvedDatabaseAuthPropertiesException(final String message) {
		super(message);
	}
}
