<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Secret Manager - registration</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link href="${pageContext.request.contextPath}/webjars/bootstrap/4.4.1-1/css/bootstrap.min.css" type="text/css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="css/styles.css"/>
</head>

<body>
<div id="main-container">
    <nav class="navbar navbar-collapse navbar-dark bg-dark">
        <div class="">
            <a class="navbar-brand" href="#">
                Secret Manager
            </a>
        </div>
        <div>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <button type="button" class="btn btn-success">Sign In</button>
                </li>
            </ul>
        </div>
    </nav>
    <div id="form-container" class="container">
        <form class="form-signin center" method="post" action="#">
            <h2 class="form-signin-heading">Create account</h2>
            <input name="inputEmail" type="email" class="form-control" placeholder="Email" required autofocus/>
            <input name="inputUsername" type="text" class="form-control" placeholder="Username" required autofocus/>
            <input name="inputPassword" type="password" class="form-control" placeholder="Password" required/>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
        </form>
    </div>
    <footer class="py-4 bg-dark text-white-50">
        <div class="container text-center">
            <small>Secret Manager - developed by @kamil.jurczak</small>
        </div>
    </footer>
</div>
<script src="webjars/jquery/3.3.1/jquery.min.js"></script>
<script src="webjars/bootstrap/4.4.1-1/js/bootstrap.min.js"></script>
</body>
</html>