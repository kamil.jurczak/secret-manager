package pl.jurczak.kamil.entity;

import javax.persistence.*;

@Entity
@Table(name = "clients")
public class Client {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "client_id")
	private String clientId;

	@Column(name = "client_secret")
	private String clientSecret;

	@Column(name = "redirect_uri")
	private String redirectUri;

	@Column(name = "scope")
	private String scope;
}
