package pl.jurczak.kamil.entity;

import javax.persistence.*;
import java.security.Principal;

@Entity
@Table(name = "users")
public class User implements Principal {

	public User(final String username, final String email, final String password) {
		this.username = username;
		this.email = email;
		this.password = password;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id")
	private String userId;

	@Column(name = "password")
	private String password;

	@Column(name = "roles")
	private String roles;

	@Column(name = "scopes")
	private String scopes;

	@Column(name = "username")
	private String username;

	@Column(name = "email")
	private String email;

	@Override
	public String getName() {
		return userId;
	}
}
