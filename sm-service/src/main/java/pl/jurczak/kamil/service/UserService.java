package pl.jurczak.kamil.service;

import pl.jurczak.kamil.dao.DAOFactory;
import pl.jurczak.kamil.entity.User;

public class UserService {

	public void addUser(String username, String email, String password) {
		final var userDAO = DAOFactory.getUserDAO();
		userDAO.create(new User(username, email, password));
	}
}
